package default

fun foo(supersedingStuff: List<String>): List<String> {
    return supersedingStuff.map { it } 
}